import React from 'react';
import { View } from 'react-native';
import Button from './Button';

type CalculatorButton = {
    orange?: boolean
    wide?: boolean
    name: string
}
interface ButtonRowProps {
    buttons: CalculatorButton[]
    handleButtonPress: (name: string) => void
}
  
const ButtonRow = ({ buttons, handleButtonPress }: ButtonRowProps) => {
    const mapButtons = () => {
        return buttons.map((button, i) => {
            return <Button
                key={i}
                name={button.name}
                orange={button.orange}
                wide={button.wide}
                handleButtonPress={handleButtonPress}
            />
        });
    };

    return (
        <View style={{ flexDirection: "row"}}>
            {mapButtons()}
        </View>
    )
};

export default ButtonRow;