import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles/Display.style';
interface DisplayProps {
    value: string
}

const Display = ({value}: DisplayProps) => {
    return (
        <View testID={'display'}>
            <Text style={styles.value}>{value}</Text>
        </View>
    );
}

export default Display;