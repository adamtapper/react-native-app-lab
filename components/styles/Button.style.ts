import { StyleSheet, Dimensions } from 'react-native';

const screen = Dimensions.get("window");
const buttonWidth = screen.width / 4;
export default StyleSheet.create({
    text: {
        color: "#fff",
        fontSize: 25
    },
    button: {
        backgroundColor: "#e0e0e0",
        flex: 1,
        height: buttonWidth - 10,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: buttonWidth,
        margin: 5
    },
    buttonDouble: {
        width: screen.width / 2 -10,
        flex: 0,
        alignItems: "flex-start",
        paddingLeft: 40
    },
    buttonAccent: {
        backgroundColor: "#f5923e"
    }

});