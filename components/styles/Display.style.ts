import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    value: {
        color: "#fff",
        fontSize: 100,
        textAlign: "right",
        marginRight: 20,
        marginBottom: 10
    }
  });