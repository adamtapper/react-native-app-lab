import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import styles from './styles/Button.style';

interface ButtonProps {
    name: string
    orange?: boolean
    wide?: boolean
    handleButtonPress: (name: string) => void
  }

const Button = ({ handleButtonPress, name, wide, orange }: ButtonProps) => {
  const buttonStyles = [styles.button];
  const textStyles = [styles.text];

  if(wide) {
    buttonStyles.push(styles.buttonDouble)
  }

  if(orange) {
    buttonStyles.push(styles.buttonAccent);
  }

  return (
    <TouchableOpacity testID="button" onPress={event => handleButtonPress(name)} style={buttonStyles}>
        <Text style={textStyles}>{name}</Text>
    </TouchableOpacity>

)};

export default Button;