import React from 'react';
import { render, fireEvent } from "@testing-library/react-native";
import styles from '../styles/Button.style';
import Button from '../Button';

// Key Concept: Test Grouping
describe('Unit Tests', () => {
    test('render_buttonName_buttonNameDisplayed', () => {
        // Key Concept: capturing render in React native Testing Library
        const button = render(<Button orange={true} wide={true} handleButtonPress={jest.fn()} name={'test'} />);

        expect(button.getByText('test')).toBeTruthy();
    });

    test('handleButtonPress_clickButton_handleFunctionCalledWithButtonName', () => {
        // Key Concept: Mock Function
        const handleFn = jest.fn();
        const button = render(<Button orange={true} wide={true} handleButtonPress={handleFn} name={'test'} />);

        // Key Concept: fireEvent from React native testing library
        fireEvent.press(button.getByText('test'));

        expect(handleFn).toHaveBeenCalledTimes(1);
        // Key Concept: Pulling data from mock
        expect(handleFn.mock.calls[0][0]).toBe('test');
    });

    // Key Concept: Parameterized Tests
    test.each([
        [false, false, {...styles.button, opacity: 1}],
        [true, false, {...styles.button, ...styles.buttonAccent, opacity: 1}],
        [false, true, {...styles.button, ...styles.buttonDouble, opacity: 1}],
        [true, true, {...styles.button, ...styles.buttonAccent, ...styles.buttonDouble, opacity: 1}]
    ])('formatClassName_orange%sAndWide%s_styleAdjustDynamically', (orange, wide, expectedStyle) => { // Key Concept: Dynamic Test Naming
        const button = render(<Button orange={orange} wide={wide} handleButtonPress={jest.fn()} name={'test'} />);

        const styles = button.getByTestId('button').props.style;

        expect(styles).toEqual(expectedStyle);
    });
});