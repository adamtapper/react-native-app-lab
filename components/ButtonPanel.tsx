import React from 'react';
import { View } from 'react-native';
import ButtonRow from './ButtonRow';

interface ButtonPanelProps {
  handleButtonPress: (name: string) => void
}

const ButtonPanel = ({handleButtonPress}: ButtonPanelProps) => {
    const rows = [
      [
        {name: 'AC'},
        {name: '+/-'},
        {name: '%'},
        {name: '÷', orange: true},
      ],
      [
        {name: '7'},
        {name: '8'},
        {name: '9'},
        {name: 'x', orange: true},
      ],
      [
        {name: '4'},
        {name: '5'},
        {name: '6'},
        {name: '-', orange: true},
      ],
      [
        {name: '1'},
        {name: '2'},
        {name: '3'},
        {name: '+', orange: true},
      ],
      [
        {name: '0', wide: true},
        {name: '.'},
        {name: '=', orange: true},
      ]
    ];

    const mapButtonRows = () => {
      return rows.map((row, i) => {
        return <ButtonRow key={i} buttons={row} handleButtonPress={handleButtonPress} />
      });
    }
  
    return (
        <View>
            {mapButtonRows()}
        </View>
    );
}

  export default ButtonPanel;