import React from 'react';
import axios from 'axios';
import { render, fireEvent, within, waitFor, RenderAPI } from '@testing-library/react-native';
import App from './App';
import CalculationModel from './models/CalculationModel';
jest.mock('axios');

const axiosMock = axios.post as jest.Mock;

describe('Component Tests', () => {
  let app: RenderAPI;
  let display: HTMLElement;
  beforeEach(() => {
    app = render(<App />);
    display = app.getByTestId('display');
  });

  const enterNumber = (digits: string) => {
    digits.split('').forEach(digit => {
      if (digit !== '-') {
        const elements = app.getAllByText(digit);
        if (elements.length > 1) {
          fireEvent.press(elements[1]);
        } else {
            fireEvent.press(elements[0]);
        }
      }
    });

    if (digits.includes('-')) {
      fireEvent.press(app.getByText('+/-'));
    }
  };

  // Key Concept: Nested grouping - these tests focus on the display prior to the calculation
  describe('Display', () => {
    test('inputDecimal_decimalPressedFirst_decimalAddedToZero', () => {
      fireEvent.press(app.getByText('.'));
    
      // Key Concept: within from React native testing library
      expect(within(display).getByText('0.')).toBeTruthy();
    });

    // Task: Implement tests for inputDecimal method
    // Task: Implement tests for resetState method
    // Task: Implement tests for the percentage operand in the updateOperand method
    // Task: Implement tests for the integer operands in the updateOperand method
    // Task: Implement tests for the sign change operand in the updateOperand method
  });

  describe('Calculation', () => {
    afterEach(() => {
      // Key Concept: Clears any fake implementations but leaves the method as a mock object
      axiosMock.mockReset();
    });

    test.each([
      ['+'],
      ['-'],
      ['x'],
      ['÷']
    ])('handleoperator_%sPressedMultipleTimes_correctOperationSentToService', async (operator) => {
      enterNumber('3');

      fireEvent.press(app.getByText(operator));
      fireEvent.press(app.getByText(operator));

      enterNumber('4');
      fireEvent.press(app.getByText('='));

      // Key Concept: Asynchronous code handling in React testing library
      await waitFor(() => expect(axiosMock).toHaveBeenCalledTimes(1));

      // Key Concept: toHaveBeenCalledWith vs .mock.calls
      expect(axiosMock).toHaveBeenCalledWith(
        'https://localhost:5001/calculator',
        {
          'firstOperand': 3,
          'operator': operator,
          'secondOperand': 4
        }
      );
    });
  });
});