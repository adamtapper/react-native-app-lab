export default interface CalculationModel {
    firstOperand: number;
    secondOperand: number,
    operator: string
}