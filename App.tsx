import React, { useState } from 'react';
import { SafeAreaView, View } from 'react-native';
import ButtonPanel from './components/ButtonPanel';
import Display from './components/Display';
import CalculatorService from './services/CalculatorService';

export default function App() {
  const digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  const validOperands = [...digits, '%', '+/-', 'AC', '.'];
  const validOperators = ['+', '-', 'x', '÷'];
  const [display, setDisplay] = useState('0');
  const [firstOperand, setFirstOperand] = useState<number|null>(null);
  const [secondOperandEntered, setSecondOperandEntered] = useState<boolean>(false);
  const [operator, setOperator] = useState<string|null>(null);

  const executeCalculation = async () => {
    if (firstOperand !== null && operator !== null) {
      const response = await CalculatorService.calculate({
        firstOperand: firstOperand,
        secondOperand: Number(display),
        operator: operator
      });

      setDisplay(response.result);
      setOperator(null);
    }
  };

  const handlePress = async (buttonName: string) => {
    if (validOperands.includes(buttonName)) {
      updateOperand(buttonName);
    } else if (validOperators.includes(buttonName)) {
      handleOperator(buttonName);
    } else {
      await executeCalculation();
    }
  };

  const inputDecimal = (dot: string) => {
    if (!display.includes(dot) || secondOperandEntered) {
      if (secondOperandEntered) {
        setDisplay('0.')
        setSecondOperandEntered(false);
      } else {
        setDisplay(display + dot);
      } 
    }
  };

  const updateOperand = (buttonName: string) => {
    if (digits.includes(buttonName)) {
      if (display === '0') {
        setDisplay(buttonName);
      } else if (secondOperandEntered) {
        setDisplay(buttonName);
        setSecondOperandEntered(false);
      } else {
        setDisplay(display + buttonName);
      }
    } else if (buttonName === '.') {
      inputDecimal(buttonName);
    } else if (buttonName === '%') {
      setDisplay(`${Number(display) / 100}`);
    } else if (buttonName === '+/-') {
      setDisplay(`${-1 * Number(display)}`);
    } else {
      resetState();
    }
  }

  const handleOperator = (buttonName: string) => {
    setFirstOperand(Number(display));
    setOperator(buttonName);
    setSecondOperandEntered(true);
  };

  const resetState = () => {
    setDisplay('0');
    setFirstOperand(null);
    setSecondOperandEntered(false);
    setOperator(null);
  };

  return (
    
    <View style={{
      flex: 1,
      backgroundColor: '#202020',
      justifyContent: 'flex-end',
    }}>
      <SafeAreaView>
        <Display value={display} />
        <ButtonPanel handleButtonPress={handlePress} />
      </SafeAreaView>
    </View>
  );
}
